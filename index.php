<?php
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal ("shaun");
echo "Nama : $sheep->name <br>";
echo "Jumlah kaki : $sheep->legs <br>";
echo "Berdarah dingin : $sheep->cold_blooded <br> <br>";

$sungokong = new Ape("kera sakti");
echo "Nama : $sungokong->name <br>";
echo "Jumlah kaki : $sungokong->legs <br>";
echo "Berdarah dingin : $sungokong->cold_blooded <br>";
echo "Berbunyi : ";
echo $sungokong->yell();

$kodok = new Frog("buduk");
echo " <br><br>Nama : $kodok->name <br>";
echo "Jumlah kaki : $kodok->legs <br>";
echo "Berdarah dingin : $kodok->cold_blooded <br>";
echo "Berbunyi : ";
echo $kodok->jump();
?>