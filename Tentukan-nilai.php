<?php
function tentukan_nilai($number) {
	if ($number >= 85 && $number <= 100) {
		return  "Sangat baik";
	} elseif ($number >= 70 && $number <= 85 ) {
		return "baik";
	} elseif ($number >= 60 && $number <= 70 ) {
		return "cukup";
	} else {
		return "kurang";
	}
    
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo '<br>';
echo tentukan_nilai(76); //Baik
echo '<br>';
echo tentukan_nilai(67); //Cukup
echo '<br>';
echo tentukan_nilai(43); //Kurang
?>