<?php
function ubah_huruf($string){
	$string = strtr($string, ['a' => 'b', 'b' => 'c', 'c' => 'd', 'd' => 'e', 'e' => 'f', 'f' => 'g', 'g' => 'h', 'h' => 'i', 'i' => 'j', 'j' => 'k', 'k' => 'l', 'l' => 'm', 'm' => 'n', 'n' => 'o', 'o' => 'p', 'p' => 'q', 'q' => 'r', 'r' => 's', 's' => 't', 't' => 'u', 'u' => 'v', 'v' => 'w', 'w' => 'x', 'x' => 'y', 'y' => 'z', 'z' => 'a']);
	return $string;	

}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo '<br>';
echo ubah_huruf('developer'); // efwfmpqfs
echo '<br>';
echo ubah_huruf('laravel'); // mbsbwfm
echo '<br>';
echo ubah_huruf('keren'); // lfsfo
echo '<br>';
echo ubah_huruf('semangat'); // tfnbohbu

?>
